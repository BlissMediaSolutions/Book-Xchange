# Book-Xchange
##### The Book Xchange Web Platform - to facilitate Swinburne University students selling text books

Status: Incomplete, Under Development

Developed with HTML5, CSS3, javascript, Angular.js, AJAX/JSON, PHP & SQL.

##### Notes: 
- SQL syntax is design for use with MySQL & MariaDB
- 'settings.php' lists the connection details for the DB.  You need to modify the password in this, with your DB password
- PHP Unit Tests requires installation of PHPUnit framework
- Internet Explorer 8 (and earlier) don't support HTML5

##### Tested in: 
- Firefox 45.0.2 
- Chrome 49.0.2
- Internet Explorer 11.0.9600

##### Developed by:
- Danielle Walker (1060325)
- Terry Lewis (9998373)
- Kushani Liyanage (100056391)
   
   
==========================
"The Book-Xchange" Copyright (C) 2016, Swinburne University of Technology

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

